package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase="hello world";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("hello world", transaltor.getPhrase());
	}
	
	@Test
	public void testTransaltionEmpityPhrase() {
		String inputPhrase="";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals(Transaltor.NIL, transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithAEndingWithY() {
		String inputPhrase="any";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("anynay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithUEndingWithY() {
		String inputPhrase="utility";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("utilitynay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase="apple";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("appleyay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase="ask";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("askay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithY() {
		String inputPhrase="yogurt";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ogurtyay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithSingleConsonant() {
		String inputPhrase="hello";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ellohay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithTwoConsonant() {
		String inputPhrase="known";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ownknay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithFourConsonant() {
		String inputPhrase="skyline";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ineskylay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingMoreWordsSpace() {
		String inputPhrase="hello world";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ellohay orldway", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingMoreWordsDash() {
		String inputPhrase="well-being";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ellway-eingbay", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingContainingMoreWordsSpaceAndExclamationMark() {
		String inputPhrase="hello world!";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ellohay orldway!", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingContainingSingleWordExclamationMark() {
		String inputPhrase="known!";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ownknay!", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingContainingSingleWordAndPoint() {
		String inputPhrase="hello.";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ellohay.", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingContainingSingleWordAndRoundParenthesis() {
		String inputPhrase="(skyline)";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("(ineskylay)", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingContainingMoreWordsDashAndRoundParenthesis() {
		String inputPhrase="(well-being)";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("(ellway-eingbay)", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingContainingSingleWordsAndComma() {
		String inputPhrase="yogurt,";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ogurtyay,", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseStartingWithVowelEndingWithConsonantAndQuestionMark() {
		String inputPhrase="ask?";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("askay?", transaltor.transalte());
	}
	
	@Test
	public void testTransaltionPhraseContainingContainingMoreWordsDashAndQuestionMark() {
		String inputPhrase="well-being?";
		Transaltor transaltor=new Transaltor(inputPhrase);
		
		assertEquals("ellway-eingbay?", transaltor.transalte());
	}
	
	
}
