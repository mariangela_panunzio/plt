package plt;

public class Transaltor {
	
	public static final  String NIL="nil";
	
	private String phrase;
	private int countConsonants=0;
	
	public Transaltor(String inputPhrase) {
		phrase=inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String transalte() {
		String phrasePuntuationStart="";
		String phrasePuntuationEnd="";
		
		if(startWithPuntuation(phrase)) {
			phrasePuntuationStart=phrase.substring(0,1);
			phrase=phrase.substring(1, phrase.length());
		}
		
		if(endWithPuntuations(phrase))
		{
			phrasePuntuationEnd=phrase.substring(phrase.length()-1);
			phrase=phrase.substring(0, phrase.length()-1);
		}
		
		if(containsMoreWords()) {
			return phrasePuntuationStart+transalteAPhraseWithMoreWords()+phrasePuntuationEnd;
		}
		else {
			return phrasePuntuationStart+transaltePhraseWithASingleWord(phrase)+phrasePuntuationEnd;
		}
		
	}

	public String transaltePhraseWithASingleWord(String phraseGiven) {
		countConsonants=0;
		
		if(phraseGiven.isBlank()) return NIL;
		
		if(!startWithVowel(phraseGiven)) {
			String cosonants=getConsonants(phraseGiven);
			return phraseGiven.substring(countConsonants)+cosonants+"ay";
		}
		
		if(startWithVowel(phrase)) {
			if(phraseGiven.endsWith("y")) {
				return phraseGiven+"nay";
			}
			else if (endWithVowel(phrase)) {
				return phraseGiven+"yay";
			}else if(!endWithVowel(phrase)) {
				return phraseGiven+"ay";
			}
		}
		return NIL;
		
	}
	
	public String transalteAPhraseWithMoreWords() {
		String transaltion="";
		
		if(phrase.contains(" ")) {
			transaltion=transalteAPhraseWithMoreWordsSpaces();
		}
		else if(phrase.contains("-")) {
			transaltion=transalteAPhraseWithMoreWordsDashes();
		}
		
		return transaltion;
		
	}
	
	public String transalteAPhraseWithMoreWordsSpaces() {
			
			String[] words=getWords();
			String transaltion="";
			
			
			for(int i=0; i<words.length; i++)
			{
				if(phrase.contains(" ")) {
					transaltion+=transaltePhraseWithASingleWord(words[i])+" ";
				}
			}
			
			return transaltion.trim();
			
		}
	
	public String transalteAPhraseWithMoreWordsDashes() {
		
		String[] words=getWords();
		String transaltion="";
		
		
		for(int i=0; i<words.length; i++)
		{
			if(phrase.contains("-")) {
				if(i==words.length-1) transaltion+=transaltePhraseWithASingleWord(words[i]);
				else transaltion+=transaltePhraseWithASingleWord(words[i])+"-";
			}
		}
		
		return transaltion;
		
	}
	
	private boolean startWithVowel(String phraseGiven) {
		return phraseGiven.startsWith("a") || phraseGiven.startsWith("e") || phraseGiven.startsWith("i") || phraseGiven.startsWith("o") || phraseGiven.startsWith("u");
	}
	
	private boolean endWithVowel(String phraseGiven) {
		return phraseGiven.endsWith("a") || phraseGiven.endsWith("e") || phraseGiven.endsWith("i") || phraseGiven.endsWith("o") || phraseGiven.endsWith("u");
	}
	
	private boolean startWithPuntuation(String phraseGiven) {
		return phraseGiven.startsWith("(");
	}
	
	private boolean endWithPuntuations(String phraseGiven) {
		return phraseGiven.endsWith(".") || phraseGiven.endsWith(",") || phraseGiven.endsWith(";") || phraseGiven.endsWith("?") || phraseGiven.endsWith("!") || phraseGiven.endsWith("'") || phraseGiven.endsWith(")");
	}
	
	private String getConsonants(String phraseGiven)
	{
		String phraseUsed=phraseGiven;
		String consonants="";
		String firstChar;
		
		while(!startWithVowel(phraseUsed.substring(countConsonants)))
		{
			if(!startWithVowel(phraseUsed.substring(countConsonants))) {			
				firstChar=phraseUsed.substring(countConsonants, countConsonants+1);
				consonants=consonants+firstChar;
				countConsonants++;
			}
			
		}
		
		return consonants;
	}
	
	private boolean containsMoreWords() {
		return phrase.contains(" ") || phrase.contains("-");
		
	}
	
	private String[] getWords()
	{
		String[] words={};
		if(phrase.contains(" ")) {
			words=phrase.split(" ");
		}
		else if(phrase.contains("-")) {
			words=phrase.split("-");
		}
		
		return words;
	}

}
